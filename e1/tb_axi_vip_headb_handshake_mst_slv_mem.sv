//////////////////////////////////////////////////////////////////////////////////
// Company:         OpenSource
// Engineer:        lf_gibbs@163.com  ljgibbs@zhihu
// 
// Create Date: 2020/06/11 21:50:26
// Design Name: headbig axi4
// Module Name: tb_axi_vip_headb_handshake_mst_slv_mem
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
//              Xilinx AXI VIP testbench
//              for headbig AXI4 (E1) hello AXI handshake
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//  
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

`include "axi_vip_master_exdes_generic.sv"
`include "axi_vip_headb_mst_stimulus.sv"
`include "axi_vip_master_mem_stimulus.sv"

module tb_axi_vip_headb_handshake_mst_slv_mem(
  );
     
  // Clock signal
  bit                                     clock;
  // Reset signal
  bit                                     reset;

  // event to stop simulation
  event                                   done_event;

  axi_vip_master_exdes_generic      generic_tb();
  axi_vip_headb_mst_stimulus        mst();
  axi_vip_master_mem_stimulus       slv();

  // instantiate bd
  chip DUT(
        .aresetn(reset),
        .aclk(clock)
  );

  initial begin
    reset <= 1'b1;
  end
  
  always #10 clock <= ~clock;

endmodule
